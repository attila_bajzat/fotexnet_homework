<?php

require_once('conn.php');
require_once('crawler.php');

// Adatbazis lekerdezesek
$sql = "INSERT INTO channels (date_text,channels_name,program_start,program_title,short_description,age_limit)
VALUES (:date_text, :channels_name, :program_start, :program_title, :short_description, :age_limit )";

$selectSqlForTable = "SELECT channels_name,date_text,program_start,program_title,short_description,age_limit FROM channels WHERE date_text = :date_text AND channels_name = :channels_name";
$selectSql = "SELECT date_text FROM channels WHERE date_text = :date_text";

$toButtonDateSql = "SELECT DISTINCT date_text FROM channels";
$toButtonChannelSql = "SELECT DISTINCT channels_name FROM channels";

// 1. feladat
// Amennyiben mar van post bekerdezunk a db-be, hogy a megadott datummal vannak adatok 
if(isset($_POST['date'])) {
    $date = $_POST['date'];

    $inDb = $dbh->prepare($selectSql);
    $inDb->bindParam(":date_text", $date, PDO::PARAM_STR);

    try {
        $inDb->execute();
      } catch (Exception $e) {
        echo $e->getMessage();
      }

    if($inDb->fetchAll()) {
        die(('This date is in DB.'));
    } 

    // Amennyiben nincsen meg a post-ban szereplo datummal a db-ben abban az esetben a port.hu API-n kesztul lekerjuk a musorokat datum alapjan
    $crawler = new Crawler();
    $data =  $crawler->getData($date);
    // Az adatok az sqllite db-be kerulnek  
    foreach ($data['channels'] as $channel) {
        $channelsName = $channel['name'];
        foreach ($channel['programs'] as $program) {
            $programStart = $program['start_time'];
            $programTitle = $program['title'];
            $descrtiption = $program['short_description'];
            $ageLimit = $program['restriction']['age_limit'];

            $inserData = $dbh->prepare($sql);
            $inserData->bindParam(":date_text", $date, PDO::PARAM_STR);
            $inserData->bindParam(":channels_name", $channelsName, PDO::PARAM_STR);
            $inserData->bindParam(":program_start", $programStart, PDO::PARAM_STR);
            $inserData->bindParam(":program_title", $programTitle, PDO::PARAM_STR);
            $inserData->bindParam(":short_description", $descrtiption, PDO::PARAM_STR);
            $inserData->bindParam(":age_limit", $ageLimit, PDO::PARAM_STR);
            
            try {
                $inserData->execute();
              } catch (Exception $e) {
                die('Something went wrong.');
              }
        }
    }
}

// 2. feladat
//A gombok kihelyezesehez a db-ben megnezzuk, hogy milyen datumok es csatornak szerepelnek
$dateInDb = $dbh->prepare($toButtonDateSql);
try {
    $dateInDb->execute();
  } catch (Exception $e) {
    echo $e->getMessage();
  }
  $dateButton = $dateInDb->fetchAll();

$channelInDb = $dbh->prepare($toButtonChannelSql);
try {
    $channelInDb->execute();
  } catch (Exception $e) {
    echo $e->getMessage();
  }
  $channelButton = $channelInDb->fetchAll();

// Post-ban szereplo datum es csatorna alapjan a db-bol lekerjuk a musorokat  
if(isset($_POST['channel'])) {
    $day = $_POST['day'];
    $channel = $_POST['channel'];

    $inDb = $dbh->prepare($selectSqlForTable);
    $inDb->bindParam(":date_text", $day, PDO::PARAM_STR);
    $inDb->bindParam(":channels_name", $channel, PDO::PARAM_STR);

    try {
        $inDb->execute();
      } catch (Exception $e) {
        echo $e->getMessage();
      }
      $channelDatas = $inDb->fetchAll();
}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Port copy</title>
  </head>
  <body>
    <h1>Port.hu task</h1>
    <div class="container">
        <h3>1. task</h3>
        <div class="input-group">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                <div class="form-group">
                <label for="sel1">Select date:</label>
                <select class="form-control" name="date">
                    <option disabled selected value> -- Dates -- </option>
                    <option value="<?php echo date("Y-m-d") ?>">Today</option>
                    <option value="<?php echo date("Y-m-d", strtotime("+1 day")) ?>">Tomorrow</option>
                    <?php
                        for ($i=2; $i < 11; $i++) { 
                            echo "<option>".  date('Y-m-d', strtotime("+". $i ." day")) . "</option>";
                        }
                    ?>
                </select>
                </div>
                <input type="submit" name="submit" value="Get date" />
            </form>
        </div>
    </div>
    <div class="container">
        <h3>2. task</h3>
        <div class="input-group">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post"> 
                <div class="form-group">
                <select class="form-control" name="day">
                    <?php
                    foreach ($dateButton as $button) {
                        echo "<option value=" . $button['date_text'] . ">". $button['date_text'] ."</option>";
                    }
                    ?>
                </select>
                <select class="form-control" name="channel">
                    <?php
                    foreach ($channelButton as $button) {
                        $channelName = "'" . $button['channels_name'] . "'";
                        echo "<option value=" . $channelName . ">". $channelName ."</option>";
                    }
                    ?>
                </select>
                <input type="submit" name="submit" value="Channel" />
                </div>
            </form>
        </div>
    </div>   
    <?php
        if(isset($channelDatas)) {
    ?>
    <div class='container'>
    <table class="table table-striped">
        <thead>
            <tr>
            <th scope="col">Date_text</th>
            <th scope="col">Channels_name</th>
            <th scope="col">Program_start</th>
            <th scope="col">Program_title</th>
            <th scope="col">Short_description</th>
            <th scope="col">Age_limit</th>
            </tr>
        </thead>
        <tbody>
        <?php
            foreach ($channelDatas as $channelData) {
                echo "<tr><th scope='row'>" . $channelData['date_text'] . "</th>
                <td>" . $channelData['channels_name'] . "</td>
                <td>" . $channelData['program_start'] . "</td>
                <td>" . $channelData['program_title'] . "</td>
                <td>" . $channelData['short_description'] . "</td>
                <td>" . $channelData['age_limit'] . "</td>
                </tr>";
            }
        ?>
        </tbody>
        </table>
    </div> 
    <?php } ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>