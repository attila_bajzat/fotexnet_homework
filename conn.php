<?php

$dir = 'sqlite:data.sqlite';
$dbh  = new PDO($dir) or die("cannot open the database");

$query =  "CREATE TABLE IF NOT EXISTS channels (
    channel_id   INTEGER PRIMARY KEY AUTOINCREMENT,
    date_text   TEXT      NOT NULL,
    channels_name TEXT    NOT NULL,
    program_start TEXT    NOT NULL,
    program_title TEXT    NOT NULL,
    short_description TEXT    NOT NULL,
    age_limit INTEGER    NOT NULL
)";

$result = $dbh->exec($query);
if($result) {
    die(var_export($dbh->errorinfo(), TRUE));
}
?>